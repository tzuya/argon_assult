﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] GameObject deathFX; //Dichiaro un SerializeField a cui associerò in UNITY l'effetto da mostrare alla morte di un nemico
    [SerializeField] Transform parent; //Dichiaro un oggetto SerializeField che assegno in UNITY che contiene le nuove posizioni (0,0,0)
    [SerializeField] int scorePerHit = 10; //Dichiaro variabile SerializeField dei punti per ogni uccisione
    [SerializeField] int hitsBeforeDeath = 3; //Numero di colpi per la morte

    ScoreBoard scoreBoardScript; //Dichiaro una variabile di tipo script ScoreBoard

    // Start is called before the first frame update
    void Start()
    {
        AddNonTriggerBoxCollider(); //Funzione personalizzata (vedi sotto) che aggiunge un box collider non trigger dinamicamente all'avvio
        scoreBoardScript = FindObjectOfType<ScoreBoard>(); //Cerco l'oggetto a runtime di tipo ScoreBoard
    }

    //Funzione che aggiunge un box collider non trigger dinamicamente all'avvio
    private void AddNonTriggerBoxCollider()
    {
        Collider enemyCollider = gameObject.AddComponent<BoxCollider>(); //Aggiungo&Dichiaro dinamicamente un box colluider all'oggetto a cui è collegato lo script
        enemyCollider.isTrigger = false; //Modifico la propieta trigger del box collider
    }

    //Funzione attivata alla collisione delle PARTICLES
    private void OnParticleCollision(GameObject other)
    {
        HittedProcess(); //Funzione che viene esguita quando viene colpita la navicella nemica
        if (hitsBeforeDeath <= 0) //Se i punti vita della navicella nemica sono a 0 eseguo la funzione per farla scomparire
        {
            KillEnemy();
        }

    }

    //Funzione da usare quando viene colpita la navicella nemica
    private void HittedProcess()
    {
        scoreBoardScript.ScoreHit(scorePerHit); //eseguo la funzione per aggiornare il punteggio e passo il parametro dei punti acquisiti
        hitsBeforeDeath--;//Sottraggo un punto alla vita rimanente
    }

    //Funzione che fa scomparire le navicelle nemiche
    private void KillEnemy()
    {
        GameObject fx = Instantiate(deathFX, transform.position, Quaternion.identity); //Creo & DIchiaro una nuova istanza dell' oggetto deathFX / nella posizione attuale dell'oggetto a cui è associato lo script / senza nessuna rotazione)
        fx.transform.parent = parent; //Assegno la posizione della variabile parent all'oggetto padre della varibile fx
        Destroy(gameObject); //Distruggo la navicella colpita Destroy(gameObject) usando gameObject (g minuscola) mi riferisco all'oggetto a cui lo script è colllegato  
    }
}
