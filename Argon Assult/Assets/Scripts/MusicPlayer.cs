﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicPlayer : MonoBehaviour
{

    private void Awake()
    {
        int musicPlayers = FindObjectsOfType<MusicPlayer>().Length; //Controllo quanti script MusicPlayer sono attivi
        if (musicPlayers > 1)//Se è presente più di uno script MusicPlayer
        {
            Destroy(gameObject); //Distruggo QUESTO elemento MusicPlayer usando gameObject (g minuscola) mi riferisco all'oggetto a cui lo script è colllegato 
        }
        else //Altrimenti
        {
            DontDestroyOnLoad(gameObject); //Non distruggo l'elemento al caricamento della scena successiva
        }
    }

}
