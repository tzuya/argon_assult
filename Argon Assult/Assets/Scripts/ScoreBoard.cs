﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Necessario per interagire con i componenti UNITY UI

public class ScoreBoard : MonoBehaviour
{
    int score = 0;
    Text labelscore;

    // Start is called before the first frame update
    void Start()
    { 
        labelscore = GetComponent<Text>(); //associo a labelscore il componente UNITY Text dell'oggetto a cui è collegato lo script
        labelscore.text = score.ToString(); //Imposto la propieta .text del componente al valore di score
    }

    public void ScoreHit(int scorePerHit)
    {
        score = score + scorePerHit; //Aggiorno il punteggio
        labelscore.text = score.ToString(); //Imposto la propieta .text del componente al valore di score
    }
}
