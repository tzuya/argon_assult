﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    //Ritardo per il reload del livello
    [Tooltip("In second")][SerializeField] float levelReloadDelay = 1f;

    //Oggetto legato all'effetto della morte della navicella
    [Tooltip("Link to death effects")] [SerializeField] GameObject deathFX;
    //Funzione per le collisioni della navicella
    private void OnTriggerEnter(Collider other)
    {
        StartDeathSequence();
    }

    private void StartDeathSequence()
    {
        print("Player Trigger something");
        deathFX.SetActive(true); //Attivo gli effetti per la collisione del player
        SendMessage("OnPlayerDeath"); //Funzione per inviare messaggi tra i vari script
        Invoke("LoadLevel", levelReloadDelay); //Ricarico il livello
    }

    private void LoadLevel()
    {
        SceneManager.LoadScene(1);
    }
}
