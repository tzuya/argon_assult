﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput; //Libreria per utilizzare le funzione di cross-platform

public class PlayerController : MonoBehaviour
{
    [Header("General")]
    [Tooltip("In Meters")] [SerializeField] float xTravel = 7f; //Variabile velocità asse X, attributo [Tooltip("In m/Sec")] aggiunge tooltip in UNITY
    [Tooltip("In Meters")] [SerializeField] float yTravel = 5f; //Variabile velocità asse y, attributo [Tooltip("In m/Sec")] aggiunge tooltip in UNITY
    [SerializeField] GameObject[] guns; //Array di oggetti da collegare in UNITY alle pistole

    [Header("Travel Speed")]
    [Tooltip("In m/Sec")] [SerializeField] float xSpeed = 10f; //Variabile velocità asse X, attributo [Tooltip("In m/Sec")] aggiunge tooltip in UNITY
    [Tooltip("In m/Sec")] [SerializeField] float ySpeed = 10f; //Variabile velocità asse y, attributo [Tooltip("In m/Sec")] aggiunge tooltip in UNITY 

    [Header("Rotation Factor")]
    [SerializeField] float positionPitchFactor = -5f; //Fattore di rotazione sull'asse X allo spostamento della navicella sull'asse Y
    [SerializeField] float positionYawFactor = 5f; //Fattore di rotazione sull'asse Y allo spostamento della navicella sull'asse X

    [Header("Adjustemnt Factor for Input")]
    [SerializeField] float controlPitchFactor = -30f; //Fattore di aggiustamento della rotazione X in base alla pressione dei tasti
    [SerializeField] float controlRollFactor = -30f; //Fattore di aggiustamento della rotazione Z in base alla pressione dei tasti

    float yThrow, xThrow; //Varibili Crossplatform assegnate nella funzione ShipTranslation()
    bool isDead=false; //Flag che indica che il player è morto


    // Update is called once per frame
    void Update()
    {
        if (!isDead)
        {
            ShipRotation();
            ShipTranslation();      
            ShipBullets();
        }
    }

    //Funzione avviata alla ricezione del messaggio OnPlayerDeath dallo script CollisionHandler.cs (tramite la funzione Sendmessage() )
    void OnPlayerDeath()
    {
        print("Control Freeze");
        isDead = true;
    }

    //Funzione di rotazione della navicella
    private void ShipRotation()
    {
        //PITCH ROTATION
        float pitchDueToPosition = transform.localPosition.y * positionPitchFactor; //Variabile per rotazine asse X (assegnata alla posizione Y * Fattore di rotazione)
        float pitchDueToControlThrow = yThrow * controlPitchFactor; //Valore di rotazione adattato alla pressione dei tasti
        float pitch= pitchDueToPosition + pitchDueToControlThrow; //Creo il valore di rotazione finale

        //ROLL ROTATION
        float rollDueToControlThrow = xThrow * controlRollFactor; //Valore di rotazione adattato alla pressione dei tasti
        float roll = rollDueToControlThrow; //Creo il valore di rotazione finale

        //YAW ROTATION
        float yawDueToPosition = transform.localPosition.x * positionYawFactor; //Variabile per rotazine asse Y (assegnata alla posizione x * Fattore di rotazione)
        float yaw = yawDueToPosition; //Variabile per rotazine asse Y

        transform.localRotation = Quaternion.Euler(pitch, yaw, roll); //Metodo per assegnare una rotazione all'oggetto
    }

    //Funzione di spostamento della navicella
    private void ShipTranslation()
    {
        xThrow = CrossPlatformInputManager.GetAxis("Horizontal"); // CrossPlatformInputManager.GetAxis("Horizontal") Permette di acquisire i comandi crossplatform orizzontali (impostati in UNITY->EDIT->PROJECT SETTINGS->INPUT MANAGER)
        float xOffset = xSpeed * xThrow * Time.deltaTime; //Creo un offset in base alla velocità*il comando del joystick*time.deltaTime (refresh rate indipendent)
        float xNewRawPosition = transform.localPosition.x + xOffset; // Creo la nuova posizione sull'asse X, sommando la posizione attualle all'offset
        float ClampedXPosition = Mathf.Clamp(xNewRawPosition, -xTravel, xTravel); //Uso la funzione clamp per limitare i valori della variabile

        yThrow = CrossPlatformInputManager.GetAxis("Vertical"); // CrossPlatformInputManager.GetAxis("Vertical") Permette di acquisire i comandi crossplatform verticali (impostati in UNITY->EDIT->PROJECT SETTINGS->INPUT MANAGER)
        float yOffset = ySpeed * yThrow * Time.deltaTime; //Creo un offset in base alla velocità*il comando del joystick*time.deltaTime (refresh rate indipendent)
        float yNewRawPosition = transform.localPosition.y + yOffset; // Creo la nuova posizione sull'asse Y, sommando la posizione attualle all'offset
        float ClampedYPosition = Mathf.Clamp(yNewRawPosition, -yTravel, yTravel); //Uso la funzione clamp per limitare i valori della variabile

        transform.localPosition = new Vector3(ClampedXPosition, ClampedYPosition, transform.localPosition.z); //trasformo la posizione attuale della navicella sull'asse X e Y (lasciando invariata Z)
    }

    //Funzione che attiva gli spari
    void ShipBullets()
    {
        if (CrossPlatformInputManager.GetButton("Fire1")) //Controllo se è premuto il tasto cross platform "Fire1" (UNITY->EDIT->PROJECT SETTINGS->INPUT MANAGER)
        {
            foreach (GameObject g in guns) //Uso un foreach per scorrere l'array di gameobject
            {
                var gunEmission = g.GetComponent<ParticleSystem>().emission; //Creo una variabile che ha accesso alla funzione Emission delle ParticleSystem
                gunEmission.enabled = true; //Setto l'abilitazione della funzione Emission delle ParticleSystem
            }
        }
        else
        {
            foreach (GameObject g in guns)//Uso un foreach per scorrere l'array di gameobject
            {
                var gunEmission =g.GetComponent<ParticleSystem>().emission;//Creo una variabile che ha accesso alla funzione Emission delle ParticleSystem
                gunEmission.enabled=false;//Setto l'abilitazione della funzione Emission delle ParticleSystem
            }
        }
    }
}
